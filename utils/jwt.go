package utils

import (
	"os"
	"fmt"
	"log"
	"time"
	"github.com/golang-jwt/jwt/v4"
	"github.com/joho/godotenv"
)

func init() {
	// import env
	var err = godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
}


var JWT_SECRET = os.Getenv("JWT_SECRET")

func GetSignedToken(userId string)(*string,error){

	mySigningKey := []byte(os.Getenv("JWT_SECRET"))
	claims := &jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Unix(time.Now().Add(time.Hour*time.Duration(24)).Unix(), 0)),
		Issuer: userId,
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	signedToken, signedTokenError := token.SignedString(mySigningKey)

	if signedTokenError != nil {
		fmt.Println("Signed Token Error: ", signedTokenError)
		return nil, signedTokenError
	}

	return &signedToken, nil

}