package utils

import (
	"encoding/json"
	"log"
	"io/ioutil"
)


func ReadJSON(filePath string)[]map[string]interface{}{
	log.Printf("Reading : %v", filePath)

	data, err := ioutil.ReadFile(filePath)

	if err != nil {
		log.Fatalf("Failed to open json file: %v", err)
	}

	dataBytes := []byte(data)

	var jsonData []map[string]interface{}
	json.Unmarshal(dataBytes, &jsonData)
	// fmt.Printf("Json File Data : %+v",jsonData)

	return jsonData
}