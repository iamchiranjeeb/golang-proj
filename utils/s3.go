package utils

import (
    "github.com/aws/aws-sdk-go/aws"
    "github.com/aws/aws-sdk-go/aws/session"
    "github.com/aws/aws-sdk-go/service/s3"
    "github.com/aws/aws-sdk-go/aws/credentials"
    "github.com/aws/aws-sdk-go/service/s3/s3manager"
    "github.com/joho/godotenv"
    "github.com/google/uuid"
    "mime/multipart"
    "path/filepath"
    "context"
    "bytes"
    "sync"
    "fmt"
    "log"
    "os"
)

func init() {
	var err = godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
}

 var (
    S3Session *session.Session
    S3Error error
    s3Once sync.Once
 )

func S3ClientSession()(*session.Session, error) {

    s3Once.Do(func() {
        fmt.Printf("Getting S3 Session....\n")

        S3Session,S3Error = session.NewSession(&aws.Config{
            Region: aws.String(os.Getenv("REGION")),
            Credentials: credentials.NewStaticCredentials(
                os.Getenv("ACCESS_KEY_ID"), // access_key_id
                os.Getenv("SECRET_ACCESS_KEY"),   // secret
                ""),
        })

    })

    if S3Error != nil {
        fmt.Println("S3 Session Error : ", S3Error)
    }

    return S3Session,S3Error
}

func DeleteObjects(s *session.Session,s3Prefix string)(error){

    ctx := context.Background()
    svc := s3.New(s)
    bucketName := os.Getenv("AWS_BUCKET")

    params := &s3.ListObjectsInput {
        Bucket: aws.String(bucketName),
        Prefix: aws.String(s3Prefix),
    }

    iter := s3manager.NewDeleteListIterator(svc,params)

    if err := s3manager.NewBatchDeleteWithClient(svc).Delete(ctx, iter); err != nil {
		return err
	}

    return nil
}

func UploadFileToS3(s *session.Session, file multipart.File, fileHeader *multipart.FileHeader,uid string)(string, error){

    size := fileHeader.Size
    buffer := make([]byte, size)
    file.Read(buffer)
    contentType := fileHeader.Header.Get("Content-Type")

    tempFileName := "Users/Profile-Pictures/" + uid +  filepath.Ext(fileHeader.Filename)

    _, err := s3.New(s).PutObject(&s3.PutObjectInput{
        Bucket:               aws.String(os.Getenv("AWS_BUCKET")),
        Key:                  aws.String(tempFileName),
        ACL:                  aws.String("public-read"),// could be private if you want it to be access by only authorized users
        Body:                 bytes.NewReader(buffer),
        ContentLength:        aws.Int64(int64(size)),
        ContentType:        aws.String(contentType),
        ContentDisposition:   aws.String("attachment"),
        ServerSideEncryption: aws.String("AES256"),
     })

     objectURL := "https://" + os.Getenv("BUCKET") + ".s3." + os.Getenv("REGION") + ".amazonaws.com/" + tempFileName

     if err != nil {
        return "", err
     }   
     return objectURL, err
}


func UploadRecipeDatas(s *session.Session,fileHeader *multipart.FileHeader,fileDirPrefix string)(*string,error){
    
    uploader := s3manager.NewUploader(s)
    contentType := fileHeader.Header.Get("Content-Type")

    f, err := fileHeader.Open()
    if err != nil {
        return nil, err
    }
    defer f.Close()

    newUUID := uuid.New()
	var uniqueID string = newUUID.String()
    fileName := uniqueID + filepath.Ext(fileHeader.Filename)

    tempFileName := fmt.Sprintf("Recipe/%v/%v",fileDirPrefix,fileName)

    result, err := uploader.Upload(&s3manager.UploadInput{
        Bucket: aws.String(os.Getenv("AWS_BUCKET")),
        Key:    aws.String(tempFileName),
        Body:   f,
        ACL:    aws.String("public-read"),
        ContentType:        aws.String(contentType),
    })


    if err != nil {
        return nil,err
    }

    return &result.Location, nil
}