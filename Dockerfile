FROM golang:1.20.3

WORKDIR /go_app

COPY . /go_app

RUN chmod +x /go_app/db_config.sh

# Git is required for fetching the dependencies.
RUN apt-get update && apt-get install -y git && apt-get install -y netcat

RUN go mod download && go build