CREATE TABLE IF NOT EXISTS ingredients(  
   code text PRIMARY KEY,
   name text,
   description text,
);

UPDATE ingredients SET description = 'ii'  WHERE code = 'code01' and id = 32fa0cbb-acc5-4a6a-b2e6-3d88f1a8f7c6;

DROP TABLE IF EXISTS ingredients;