package router

import (
	"github.com/gorilla/mux"
	"com.api/controllers"
	"com.api/middleware"
)

func Router() *mux.Router {

	router := mux.NewRouter()

	router.HandleFunc("/api/signup",controllers.CreateUser).Methods("POST")
	router.HandleFunc("/api/login",controllers.LoginUser).Methods("POST")

	userRouters := router.PathPrefix("/api").Subrouter()
	userRouters.Use(middleware.JWTMiddleware)
	userRouters.HandleFunc("/profile/pic",controllers.UploadProfilePicture).Methods("POST")
	userRouters.HandleFunc("/users/",controllers.GetAllUsers).Methods("GET")
	userRouters.HandleFunc("/delete/user/",controllers.DeletAccount).Methods("DELETE")
	

	recipeRouters := router.PathPrefix("/api").Subrouter()
	recipeRouters.Use(middleware.JWTMiddleware)
	recipeRouters.HandleFunc("/add/recipe",controllers.CreateRecipe).Methods("POST")
	recipeRouters.HandleFunc("/add/recipe/datas/{id}",controllers.AddRecipeData).Methods("POST")
	recipeRouters.HandleFunc("/view/recipes",controllers.GetAllRecipes).Methods("GET")
	recipeRouters.HandleFunc("/delete/recipe/{id}",controllers.DeleteOneRecipe).Methods("DELETE")
	recipeRouters.HandleFunc("/update/recipe/{id}",controllers.UpdateRecipe).Methods("PATCH")


	return router
}