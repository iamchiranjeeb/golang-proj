package db

import (
	"fmt"
	"log"
	"context"
	"sync"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	RecipeCollection *mongo.Collection
	mongoOnce sync.Once
)

func ConnectMongoDB()(*mongo.Collection) {
	mongoOnce.Do(func() {
		// const connString = "mongodb://127.0.0.1:27017/"
		const connString = "mongodb://mongo:27017/"
		const dbName = "GoTest"
		const colName = "receipies"

		//client option
		clientOption := options.Client().ApplyURI(connString)
		client,err := mongo.Connect(context.TODO(), clientOption)

		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("Connected to MongoDB...")

		RecipeCollection = client.Database(dbName).Collection(colName)

		//collection instance
		fmt.Println("Collection instance is ready.")
	})

	return RecipeCollection
}