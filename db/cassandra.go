package db

import (
	"fmt"
	"sync"
	"github.com/gocql/gocql"
)

var (
	Session *gocql.Session
	once sync.Once
)

func ConnectCassandra()(*gocql.Session,error){
	var err error

	once.Do(func() {
		// var err error 
		cluster := gocql.NewCluster("cassandra")
		// cluster.Keyspace = "test_go"
		Session, err = cluster.CreateSession()
		if err != nil {
			// return nil,err
			Session=nil
			// err = err
		}else {err=nil}
		fmt.Println("Connected to Cassandra DB....")

	})
	return Session, err
}
