#!/bin/bash

while ! nc -z mongo 27017 ; do
    echo "Waiting for the Mongo Server......"
    sleep 15
done

echo "Connected to Mongo Server......"

sleep 120

while ! nc -z cassandra 9042 ; do
    echo "Waiting for the Cassandra Server......"
    sleep 20
done

echo "Cassandra server is up.."
exec "$@"
