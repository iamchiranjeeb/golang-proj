package services

import (
	"com.api/db"
	"com.api/utils"
	"com.api/models"
	"com.api/constants"
	"github.com/gocql/gocql"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"go.mongodb.org/mongo-driver/bson"
	"math/big"
	"context"
	"fmt"
)


func InsertUser(NewUser models.CreateUser)(*models.CreateUser,*string,error){

	newUUID := uuid.New()
	var userId string = newUUID.String()

	password, hashPasswordError := bcrypt.GenerateFromPassword([]byte(NewUser.Password), constants.HashConst)

	if hashPasswordError != nil {
		return nil,nil,hashPasswordError
	}

	NewUser.Password = string(password)


	if err := db.Session.Query("INSERT INTO my_key.user (id, user_name, user_phone, user_email, password, profile_pic) VALUES(?,?,?,?,?,?)",
	userId,NewUser.Name, NewUser.Phone, NewUser.Email, NewUser.Password, "").Exec(); err != nil {
		fmt.Println("Error While Inserting Data")
		return nil,nil, err
	}


	return &NewUser,&userId,nil
}


func GetUsers()([]models.SingleUser){

	m := map[string]interface{}{}
	var user []models.SingleUser

	iter := db.Session.Query("SELECT * FROM my_key.user;").Iter()

	// fmt.Println(iter)
	for iter.MapScan(m){

		user = append(user,models.SingleUser{
			Name:m["user_name"].(string),
			Phone:m["user_phone"].(*big.Int),
			Email:m["user_email"].(string),
			ProfilePic:m["profile_pic"].(string),
		})

		m = map[string]interface{}{}

	}

	// fmt.Printf("%+v",user);
	return user
}


func GetUserFromEmail(email string)([]models.Users){

	m := map[string]interface{}{}
	var user []models.Users

	iter := db.Session.Query("SELECT * FROM my_key.user WHERE user_email=? ALLOW FILTERING", email).Iter()

	// fmt.Println(iter)
	for iter.MapScan(m){
		fmt.Println(m)

		user = append(user,models.Users{
			ID:m["id"].(gocql.UUID),
			Name:m["user_name"].(string),
			Phone:m["user_phone"].(*big.Int),
			Email:m["user_email"].(string),
			Password:m["password"].(string),
		})

		m = map[string]interface{}{}

	}

	fmt.Printf("%+v",user);
	return user

}

func GetUserFromID(id string)([]models.Users){

	m := map[string]interface{}{}
	var user []models.Users

	iter := db.Session.Query("SELECT * FROM my_key.user WHERE id=? ALLOW FILTERING", id).Iter()

	for iter.MapScan(m){
		fmt.Println(m)

		user = append(user,models.Users{
			ID:m["id"].(gocql.UUID),
			Name:m["user_name"].(string),
			Phone:m["user_phone"].(*big.Int),
			Email:m["user_email"].(string),
			Password:m["password"].(string),
			ProfilePic:m["profile_pic"].(string),
		})

		m = map[string]interface{}{}

	}

	return user

}

func UpdateProfilePic(id string,photo_url string)(error){

	err := db.Session.Query("UPDATE my_key.user SET profile_pic=? WHERE id=?",photo_url,id).Exec()

	return err
}


func DeleteOneAccount(id string)(error){

	dbError := db.Session.Query("DELETE my_key.FROM user WHERE id=?",id).Exec()
	filter := bson.M{"owner":id}

	delRes,recipeErr := db.RecipeCollection.DeleteMany(context.Background(),filter,nil)

	if recipeErr != nil {
		fmt.Println("Error deleting Recipes :",recipeErr)
	}

	fmt.Println("Error is ",utils.S3Error)

	s3Prefix := fmt.Sprintf("Users/Profile-Pictures/%v",id)
	utils.DeleteObjects(utils.S3Session,s3Prefix)

	fmt.Println("Recipe Deelete",delRes.DeletedCount)

	return dbError
}