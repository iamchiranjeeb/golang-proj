package services

import (
	"com.api/db"
	"com.api/models"
)


func GetAllIngredients()([]models.Ingredient){

	m := map[string]interface{}{}
	var ingredients []models.Ingredient

	iter := db.Session.Query("SELECT * FROM my_key.ingredients;").Iter()

	for iter.MapScan(m){
		ingredients = append(ingredients,models.Ingredient{
			Code:m["code"].(string),
			Name:m["name"].(string),
			Description:m["description"].(string),
		})
		m = map[string]interface{}{}
	}

	return ingredients
}