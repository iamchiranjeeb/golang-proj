package services

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"com.api/middleware"
	"com.api/models"
	"mime/multipart"
	"com.api/utils"
	"com.api/db"
	"context"
	"fmt"
)

func InsertRecipe(recipe *models.Recipes)(*mongo.InsertOneResult,error){

	inserted,err := db.RecipeCollection.InsertOne(context.Background(),*recipe)

	if err != nil {
		return nil,err
	}

	return inserted,nil
}


func GetRecipies(userID string)([]primitive.M,error){

	filter := bson.M{"owner":userID}

	var recipes []primitive.M
	curr,err := db.RecipeCollection.Find(context.Background(),filter)

	if err != nil {
		return nil,err
	}

	for curr.Next(context.Background()) {

		var recipe bson.M

		if err := curr.Decode(&recipe); err != nil {
			return nil, err
		}
		recipes = append(recipes, recipe)

	}

	return recipes,nil
}


func DeleteSingleRecipe(recipeID string)(*mongo.DeleteResult,error){

	id,err := primitive.ObjectIDFromHex(recipeID)

	if err != nil {
		return nil,err
	}

	filter := bson.M{"_id":id}

	result,err := db.RecipeCollection.DeleteOne(context.Background(),filter)

	if err != nil {
		return nil,err
	}

	return result,nil
}


func UpdateSingleRecipe(recipeId string,ownerId string,Recipe models.Recipes)(*mongo.UpdateResult,error){

	id,_ := primitive.ObjectIDFromHex(recipeId)
	filter := bson.M{"_id":id,"owner":ownerId}

	presentIngredients := GetAllIngredients()
	NewIngredients,err := middleware.IngredientValidation(Recipe.Ingredients,presentIngredients)

	if err != nil {
		return nil,err
	}

	updateQuery := bson.M{"$set":bson.M{"title":Recipe.Title,"steps":Recipe.Steps,"ingredients":NewIngredients}}

	updatedRecipe,err := db.RecipeCollection.UpdateOne(context.Background(),filter,updateQuery)

	if err != nil {
		return nil,err
	}

	if updatedRecipe.ModifiedCount == 0 {
		return nil,&utils.CustomError{Err:"No Recipe Updated."}
	}

	return updatedRecipe,nil
}


func findOneRecipe(filter primitive.M)(*models.Recipes,error){

	var recipe *models.Recipes

	recipeResult := db.RecipeCollection.FindOne(context.Background(),filter)

	if recipeResult.Err() != nil {
		return nil,utils.CustomError{Err:fmt.Sprintf("No Recipe Available with these cred")}
	}

	if decodeError := recipeResult.Decode(&recipe); decodeError != nil {
		fmt.Println(decodeError)
		return nil,utils.CustomError{Err:decodeError.Error()}
	}

	return recipe, nil
}


func UploadRecipeToS3(files []*multipart.FileHeader,ownerID string,recipeId string)([]models.RecipeData,[]error){

	var newDatas []models.RecipeData
	var myErrors [] error
	fileDirPrefix := fmt.Sprintf("%v/%v",ownerID,recipeId)
	id,_ := primitive.ObjectIDFromHex(recipeId)
	filter := bson.M{"_id":id,"owner":ownerID}

	_,findRecipeError := findOneRecipe(filter)

	if findRecipeError != nil {
		myErrors = append(myErrors,
			findRecipeError,
		)
		return newDatas, myErrors
	}

	for _,file := range files {
		loc,err := utils.UploadRecipeDatas(utils.S3Session,file,fileDirPrefix)

		if err != nil {
			msg := fmt.Sprintf("Error Occured While Uploading %v : %v",file.Filename,err)
			myErrors = append(myErrors,
				utils.CustomError{Err:msg},
			)
		}else{
			d1 := models.RecipeData{
				FileUrl:loc,
				Owner:ownerID,
			}
			newDatas = append(newDatas,d1)
		}

	}

	updateQuery := bson.M{"$set":bson.M{"data":newDatas}}
	_,err := db.RecipeCollection.UpdateOne(context.Background(),filter,updateQuery)

	if err != nil {
		myErrors = append(myErrors, err)
	}

	return newDatas,myErrors

}