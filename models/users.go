package models

import (
	"math/big"
	"github.com/gocql/gocql"
)


type Users struct {
	ID gocql.UUID `json:"id"`
	Name string `json:"name"`
	Phone *big.Int `json:"phone"`
	Email string `json:"email"`
	Password string `json:"password"`
	ProfilePic string `json:"profile_pic"`
}


type CreateUser struct {
	Name string `json:"name"`
	Phone string `json:"phone"`
	Email string `json:"email"`
	Password string `json:"password"`
	ConfirmPassword string `json:"confirm_password"`
	ProfilePic string `json:"profile_pic"`
}

type SingleUser struct {
	Name string `json:"name"`
	Phone *big.Int `json:"phone"`
	Email string `json:"email"`
	ProfilePic string `json:"profile_pic"`
}


type MultipleUsers struct {
	AllUsers []Users `json:"allUsers"`
}