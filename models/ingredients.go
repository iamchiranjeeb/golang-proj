package models


type Ingredient struct {
    Code        string     `json:"code"`
    Name        string     `json:"name"`
    Description string     `json:"description"`
    Quantity    string     `json:"quantity"`
}
