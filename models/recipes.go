package models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)


type RecipeData struct {
    DataId  primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
    FileUrl *string             `json:"fileUrl"`
    Owner   string             `json:"owner"`
}


type Recipes struct {
    ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
    Title       string             `json:"title"`
    Ingredients []Ingredient       `json:"ingredients"`
    Steps       []string           `json:"steps"`
    Data        []RecipeData       `json:"data"`
    Owner       string             `json:"owner"`
}

