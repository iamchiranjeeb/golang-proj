package controllers

import (
	"fmt"
	// "bytes"
	"net/http"
	// "mime/multipart"
	"encoding/json"
	"com.api/models"
	"com.api/services"
	"com.api/middleware"
	"github.com/gorilla/mux"
)


// The boundary string is a unique identifier that separates the different parts of the message. It is typically a randomly generated string that is unlikely to occur in the content of the message itself.



func AddRecipeData(w http.ResponseWriter, r *http.Request){

	// Create a buffer to store the multipart message
	// body := &bytes.Buffer{}
	// // Create a multipart writer and set the boundary
	// writer := multipart.NewWriter(body)
	// boundary := writer.Boundary()
	contentType := "multipart/form-data; boundary=boundary123"
	w.Header().Set("Content-Type", contentType)

	var statusCode int = http.StatusCreated
	ownerID := r.Context().Value("userId").(string)
	params := mux.Vars(r)

	resp := make(map[string]interface{})
	resp["success"] = true

	maxSize := int64(1024000*15)

	err := r.ParseMultipartForm(maxSize)
	if err != nil {
		fmt.Println("Error is here....",err)
		resp["success"] = false
		resp["result"] = fmt.Sprintf("Image too large. Max Size: %v", maxSize)
		resp["error"] = err.Error()
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	f := r.MultipartForm.File["files"]
	t,errList := services.UploadRecipeToS3(f,ownerID,params["id"])


	if len(errList) > 0 {
		resp["error"] = errList
		resp["success"] = false
		statusCode = http.StatusBadRequest
	}

	resp["result"] = t

	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(resp)
}



func CreateRecipe(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")

	ownerID := r.Context().Value("userId").(string)
	var Recipe models.Recipes
	resp := make(map[string]interface{})

	if err := json.NewDecoder(r.Body).Decode(&Recipe); err != nil {
		resp["success"] = false
		resp["result"] = "Error While Decoding Request Body"
		resp["error"] = err
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}


	if len(Recipe.Ingredients) == 0 {
		resp["success"] = false
		resp["result"] = "Error While Adding Recipe"
		resp["error"] = "Ingredients Can not be NIL"
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	allIngredients := services.GetAllIngredients()
	NewIngredients,validError := middleware.IngredientValidation(Recipe.Ingredients,allIngredients); 

	
	if validError != nil {
		resp["success"] = false
		resp["result"] = "Ingredient Validation Failed"
		resp["error"] = validError.Error()
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	Recipe.Owner = ownerID
	Recipe.Ingredients = NewIngredients

	inserted,err := services.InsertRecipe(&Recipe)

	if err != nil {
		resp["success"] = false
		resp["result"] = "Error While Adding Recipe"
		resp["error"] = err
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["message"] = "Added New Recipe"
	resp["result"] = inserted

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)
}

func GetAllRecipes(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "application/json")

	ownerID := r.Context().Value("userId").(string)
	resp := make(map[string]interface{})

	allRecipies,err := services.GetRecipies(ownerID)

	if err != nil {
		resp["success"] = false
		resp["result"] = "Error While Retriving Recipes"
		resp["error"] = err
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["result"] = allRecipies
	resp["total"] = len(allRecipies)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}


func DeleteOneRecipe(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "application/json")

	resp := make(map[string]interface{})
	params := mux.Vars(r)

	deleteRes,err := services.DeleteSingleRecipe(params["id"])

	if err != nil {
		resp["error"] = err.Error()
		resp["success"] = false
		resp["result"] = "Recipe Could not be deleted"
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	fmt.Println(deleteRes.DeletedCount)

	if deleteRes.DeletedCount == 0 {
		resp["error"] = fmt.Sprintf("No Recipe Deleted with ID %v",params["id"])
		resp["success"] = false
		resp["result"] = "No Recipe got deleted."
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["message"] = "Recipe Could not be deleted"
	resp["result"] = deleteRes

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}


func UpdateRecipe(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")

	resp := make(map[string]interface{})
	params := mux.Vars(r)
	ownerID := r.Context().Value("userId").(string)
	var Recipe models.Recipes

	if err := json.NewDecoder(r.Body).Decode(&Recipe); err != nil {
		resp["success"] = false
		resp["result"] = "Error While Decoding Request Body"
		resp["error"] = err
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	updateResult,err := services.UpdateSingleRecipe(params["id"],ownerID,Recipe)

	if err != nil {
		resp["success"] = false
		resp["result"] = "Error While Updating Recipe"
		resp["error"] = err.Error()
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["result"] = updateResult

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)

}