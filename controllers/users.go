package controllers


import (
	"net/http"
	"com.api/models"
	"com.api/services"
	"com.api/utils"
	"encoding/json"
	"io/ioutil"
	"golang.org/x/crypto/bcrypt"
	"fmt"
)

func UploadProfilePicture(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "multipart/form-data")
	resp := make(map[string]interface{})
	uid := r.Context().Value("userId").(string)
	requestUser := services.GetUserFromID(uid)
	fmt.Println("Requesting")
	fmt.Println(requestUser[0])

	if (len(requestUser) == 0){
		resp["success"] = false
		resp["result"] = "Error While Fetching User Data"
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}


	maxSize := int64(1024000*5)

	err := r.ParseMultipartForm(maxSize)
	if err != nil {
		fmt.Println("Error is here....",err)
		resp["success"] = false
		resp["result"] = fmt.Sprintf("Image too large. Max Size: %v", maxSize)
		resp["error"] = err.Error()
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	file,fileHeader,fileError := r.FormFile("profile_picture")

	if fileError != nil {
		fmt.Println("File Error",fileError)
		resp["success"] = false
		resp["error"] = fileError
		resp["result"] = fmt.Sprintf("Profile Photo Could nt be uploaded")
		fmt.Println("Profile Picture Could not be Uploaded")
		return
	}
	fmt.Println(fileHeader.Size)

	defer file.Close()

	if utils.S3Error != nil {
		fmt.Println("Error is ",utils.S3Error)
		resp["success"] = false
		resp["error"] = utils.S3Error
		resp["result"] = fmt.Sprintf("Profile Photo Could nt be uploaded")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	fileName, err := utils.UploadFileToS3(utils.S3Session, file, fileHeader, uid)

	if err != nil {
		fmt.Println("Error is 73",err)
		resp["success"] = false
		resp["error"] = err
		resp["result"] = fmt.Sprintf("Profile Photo Could nt be uploaded")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	if photoUploadErr := services.UpdateProfilePic(uid,fileName); photoUploadErr != nil {
		resp["success"] = false
		resp["error"] = photoUploadErr
		resp["result"] = fmt.Sprintf("Profile Photo Could nt be uploaded")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["result"] = fileName
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)
}


func CreateUser(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "application/json")

	var NewUser *models.CreateUser
	var err error
	var ReqUser models.CreateUser
	resp := make(map[string]interface{})

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "wrong data")
	}
	json.Unmarshal(reqBody, &ReqUser)

	if ReqUser.Password != ReqUser.ConfirmPassword {
		resp["success"] = false
		resp["result"] = fmt.Sprintf("Password && Confirm-Password did not match.")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	ifUser := services.GetUserFromEmail(ReqUser.Email)

	if len(ifUser) > 0 {
		resp["success"] = false
		resp["result"] = fmt.Sprintf("User Present with Email %s",ReqUser.Email)
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	NewUser,id,err := services.InsertUser(ReqUser)

	if err != nil {
		resp["success"] = false
		resp["result"] = "Failed To Create User"
		resp["error"] = err.Error()

		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	signedToken,tokenError := utils.GetSignedToken(*id)

	if tokenError != nil {
		resp["success"] = false
		resp["result"] = "Error While Getting Signed Token"
		resp["error"] = tokenError.Error()

		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["token"] = signedToken
	resp["result"] = *NewUser

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(resp)


}

func LoginUser(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type", "application/json")
	var ReqUser models.Users
	jsonParseError := json.NewDecoder(r.Body).Decode(&ReqUser)

	if jsonParseError != nil {
		resp := make(map[string]interface{})
		resp["success"] = false
		resp["result"] = "Error While Parsing Request Body"
		resp["error"] = jsonParseError.Error()

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	if ReqUser.Email == ""|| ReqUser.Password == ""{
		resp := make(map[string]interface{})
		resp["success"] = false
		resp["result"] = "Login Failed, Both Email && Password are required"

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	User := services.GetUserFromEmail(ReqUser.Email)

	if len(User) == 0 {
		resp := make(map[string]interface{})
		resp["success"] = false
		resp["result"] = fmt.Sprintf("User Not Found With Email: %s",ReqUser.Email)

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	passwordMatchError := bcrypt.CompareHashAndPassword([]byte(User[0].Password),[]byte(ReqUser.Password))

	if passwordMatchError != nil {
		resp := make(map[string]interface{})
		resp["success"] = false
		resp["result"] = "Email/Password is wrong"

		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(resp)
		return
	}

	id := User[0].ID.String()
	signedToken,tokenError := utils.GetSignedToken(id)

	if tokenError != nil {
		resp := make(map[string]interface{})
		resp["success"] = false
		resp["result"] = "Error While Getting Signed Token"
		resp["error"] = tokenError.Error()

		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp := make(map[string]interface{})
	resp["success"] = true
	resp["token"] = signedToken
	resp["email"] = User[0].Email

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)

}


func GetAllUsers(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]interface{})
	uid := r.Context().Value("userId").(string)
	requestUser := services.GetUserFromID(uid)

	if (len(requestUser) == 0){
		resp["success"] = true
		resp["result"] = "Error While Fetching User Data"
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	allUsers := services.GetUsers()
	resp["success"] = true
	resp["result"] = allUsers

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)
}


func GetSingleUserByEmail(w http.ResponseWriter, r *http.Request){

	var NewUser models.CreateUser
	reqBody, _ := ioutil.ReadAll(r.Body)
	json.Unmarshal(reqBody, &NewUser)
	fmt.Println(NewUser.Email)


	users := services.GetUserFromEmail(NewUser.Email)

	response := make(map[string]interface{})
	response["success"] = true
	if users == nil {
		response["result"] = "No users found"
	}else{
		response["result"] = users[0]
		fmt.Println(len(users))
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(response)

}


func DeletAccount(w http.ResponseWriter, r *http.Request){

	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]interface{})
	uid := r.Context().Value("userId").(string)

	err := services.DeleteOneAccount(uid)

	if err != nil {
		resp["success"] = false
		resp["result"] = "Error While Deleting User."
		resp["error"] = err.Error()

		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(resp)
		return
	}

	resp["success"] = true
	resp["result"] = "User Deleted."

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(resp)

}