package main

import (
	"fmt"
	"sync"
	"log"
	"net/http"
	"com.api/db"
	"com.api/router"
	"com.api/utils"
	"github.com/gocql/gocql"
)

var (
	wg sync.WaitGroup
	mut   sync.Mutex
)


func CreateTable(){

	defer wg.Done()
	fmt.Println("Creating Table")
	mut.Lock()
	Session,err := db.ConnectCassandra()

	if err != nil {
		fmt.Printf("Here 01 \n")
		panic(err)
	}

	keySpace:="my_key"
	replication := "{'class': 'SimpleStrategy', 'replication_factor': 1}"
	query := fmt.Sprintf("CREATE KEYSPACE IF NOT EXISTS %s WITH REPLICATION = %s", keySpace, replication)

	if errors := Session.Query(query).Exec(); errors != nil {
		fmt.Println("Error While Creating user Table")
		fmt.Println(errors)
		panic(errors)
	}

	if errors := Session.Query("CREATE TABLE IF NOT EXISTS my_key.user(id uuid PRIMARY KEY,user_name text,user_phone varint,user_email text,profile_pic text,password text);").Exec(); errors != nil {
		fmt.Println("Error While Creating user Table")
		fmt.Println(errors)
		panic(errors)
	}

	if errors := Session.Query("CREATE TABLE IF NOT EXISTS my_key.ingredients(code text PRIMARY KEY, name text, description text);").Exec();errors != nil {
		fmt.Println("Error While Creating ingredients Table")
		fmt.Println(errors)
		panic(errors)
	}

	batch := Session.NewBatch(gocql.LoggedBatch)
	ingredientsData := utils.ReadJSON("./metadata/ingredients.json")
	initialData := ingredientsData[0]

	var count int
	if errors := Session.Query("SELECT COUNT(*) FROM my_key.ingredients").Scan(&count); errors != nil {
		fmt.Println("Error While Counting Ingredients Table Data")
		fmt.Println(errors)
		panic(errors)
	}

	if count == 0 {
		for key,val := range initialData {
			valueString := fmt.Sprintf("%v",val)
			keyString := fmt.Sprintf("%v",key)
	
			batch.Query("INSERT INTO my_key.ingredients(code,name) VALUES (?,?)",keyString,valueString)
		}
		if err := Session.ExecuteBatch(batch); err != nil {
			fmt.Println("Error While doing Batch Entry")
			fmt.Println(err)
			panic(err)
		}
	}else{

		for key,val := range initialData {

			valueString := fmt.Sprintf("%v",val)
			keyString := fmt.Sprintf("%v",key)
			
			var singleIngredientCount int = 0;

			if errors := Session.Query("SELECT COUNT(*) FROM my_key.ingredients WHERE code = ? ALLOW FILTERING",keyString).Scan(&singleIngredientCount); errors != nil {
				fmt.Println("Error While Counting Ingredients Single row data")
				fmt.Println(errors)
				panic(errors)
			}

			if singleIngredientCount == 0 {

				if err:= Session.Query("INSERT INTO my_key.ingredients(code,name) VALUES (?,?)",keyString,valueString).Exec(); err != nil {
					fmt.Println("Error While Inserting Single Value to ingredient table.")
					fmt.Println(err)
					panic(err)
				}

			}
		}
	}

	newBatch := Session.NewBatch(gocql.LoggedBatch)
	descriptionDatas := ingredientsData[1];

	for key, val := range descriptionDatas {
		valueString := fmt.Sprintf("%v",val)
		keyString := fmt.Sprintf("%v",key)
		newBatch.Query("UPDATE my_key.ingredients SET description = ? WHERE code = ?",valueString,keyString)
	}

	if err := Session.ExecuteBatch(newBatch); err != nil {
		fmt.Println("Error While Batch Descriptions")
		fmt.Println(err)
		panic(err)
	}


	mut.Unlock()

	fmt.Println("Table Created.")
}

func ConnectMongo(){
	defer wg.Done()
	db.ConnectMongoDB()
}

func CreateS3Sessions(){
	defer wg.Done()
	utils.S3ClientSession()
}


func main() {

	wg.Add(3)
	go CreateTable()
	go ConnectMongo()
	go CreateS3Sessions()
	wg.Wait()

	r := router.Router()
	var PORT string = ":5002"
	fmt.Printf("Listening on port%s \n",PORT)
	log.Fatal(http.ListenAndServe(PORT, r))
	

	fmt.Println("Building Golang Apis")


}