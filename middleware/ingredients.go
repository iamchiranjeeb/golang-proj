package middleware

import(
	"fmt"
	"strings"
	"com.api/models"
	"com.api/utils"
)

func IngredientValidation(incoming []models.Ingredient,present []models.Ingredient)([]models.Ingredient,error){

	var presentIngredient models.Ingredient

	for idx,v := range incoming {
		fmt.Println(idx)

		val := strings.ToLower(v.Name)
		isValFound := false

		for _,p := range present {
			
			presentVal := strings.ToLower(p.Name)
			presentIngredient = p

			if presentVal == val {
				isValFound = true
				break
			}
		}

		if isValFound {
			incoming[idx].Code = presentIngredient.Code
			incoming[idx].Name = presentIngredient.Name
			incoming[idx].Description = presentIngredient.Description
		}else{
			msg := fmt.Sprintf("%v is not present in Ingredient List",v.Name)
			return nil,&utils.CustomError{Err:msg}
		}

	}
	return incoming,nil
}