package middleware

import (
	"github.com/golang-jwt/jwt/v4"
	"github.com/joho/godotenv"
	"encoding/json"
	"net/http"
	"context"
	"fmt"
	"log"
	"os"
)

func init() {
	// import env
	var err = godotenv.Load(".env")
	if err != nil {
		log.Fatal(err)
	}
}

// var JWT_SECRET = os.Getenv("JWT_SECRET")


// Define a JWTMiddleware function that takes an http.Handler as input and returns an http.Handler as output
func JWTMiddleware(next http.Handler) http.Handler {
    // Return a new http.HandlerFunc that takes an http.ResponseWriter and an http.Request as input
    return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp := make(map[string]interface{})
        // Extract the token string from the "Authorization" header in the request
        tokenString := r.Header.Get("Authorization")

		// fmt.Printf("Token String %s\n", tokenString)

        // If no token string is found, return an "Unauthorized" error response and stop processing the request
        if tokenString == "" {
            // http.Error(w, "Unauthorized", http.StatusUnauthorized)
			resp["success"] = false
			resp["error"] = fmt.Sprintf("Token Not Found.")
			resp["result"] ="Unauthorized Entry"
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(resp)
            return
        }

        // Parse the token string using the jwt.Parse function and a callback function that verifies the token's signature
        token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
            // Verify that the token's signing method is HMAC
            if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				fmt.Println(ok)
				resp["success"] = false
				resp["error"] = fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				resp["result"] ="Unauthorized Entry"
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(resp)
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
                // return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
            }
            // Return the secret key used to sign the token, which is just a byte array in this case
			// fmt.Println("JWT SEC ,",JWT_SECRET)
            return []byte(os.Getenv("JWT_SECRET")), nil // replace "secret" with your own secret key
        })

		// fmt.Println("Token Valid : ",token.Valid)

        // If there's an error parsing the token or the token is invalid, return an "Unauthorized" error response and stop processing the request
        if err != nil || !token.Valid {
			// fmt.Println(err)
            // http.Error(w, "Unauthorized", http.StatusUnauthorized)
			resp["success"] = false
			resp["error"] = fmt.Sprintf("Not Valid Token : %v", err)
			resp["result"] ="Unauthorized Entry"
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(resp)
            return
        }

		claims,ok := token.Claims.(jwt.MapClaims)

		if !ok {
			// http.Error(w, "Unauthorized", http.StatusUnauthorized)
			resp["success"] = false
			resp["error"] = fmt.Sprintf("Could not sucessed Claim %v",ok)
			resp["result"] ="Unauthorized Entry"
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(resp)
            return
		}

		userID := claims["iss"]
		ctx := context.WithValue(r.Context(), "userId", userID)

        // If the token is valid, pass the http.ResponseWriter and http.Request to the next http.Handler in the chain
        next.ServeHTTP(w, r.WithContext(ctx))
    })
}
